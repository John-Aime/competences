from django.urls import path
from api_bilan import views

urlpatterns = [
    path('api_bilan/', views.tasks),
    path('api_bilan/<int:pk>/', views.task_detail),
]