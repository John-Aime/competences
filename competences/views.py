from django.shortcuts import render
from django.http import JsonResponse
from .models import *
from rest_framework import viewsets
from .serializer import *
from rest_framework import generics


class DomaineView(viewsets.ModelViewSet):
    serializer_class = DomaineSerializer
    queryset = Domaine.objects.all()


class EvaluationView(viewsets.ModelViewSet):
    serializer_class = EvaluationSerializer
    queryset = Evaluation.objects.all()


class EmployeView(viewsets.ModelViewSet):
    serializer_class = EmployeSerializer
    queryset = Employe.objects.all()


class DepartementView(viewsets.ModelViewSet):
    serializer_class = DepartementSerializer
    queryset = Departement.objects.all()


class PosteView(viewsets.ModelViewSet):
    serializer_class = PosteSerializer
    queryset = Poste.objects.all()


class RapportView(viewsets.ModelViewSet):
    serializer_class = RapportSerializer
    queryset = Rapport.objects.all()


class ResultatView(viewsets.ModelViewSet):
    serializer_class = ResultatSerializer
    queryset = Resultat.objects.all()


class EntrepriseView(viewsets.ModelViewSet):
    serializer_class = EntrepriseSerializer
    queryset = Entreprise.objects.all()


class ConseillerView(viewsets.ModelViewSet):
    serializer_class = ConseillerSerializer
    queryset = Conseiller.objects.all()



