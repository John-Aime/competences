from django.db import models

# Create your models here.

class Domaine(models.Model):
    nom_domaine=models.CharField(max_length=100)


class Evaluation(models.Model):
    date_evaluation=models.DateField()
    domaine = models.ForeignKey(Domaine, on_delete=models.CASCADE, related_name='evaluation')


class Departement(models.Model):
    nom_departement = models.CharField(max_length=100)


class Poste(models.Model):
    nom_poste = models.CharField(max_length=100)
    departement = models.ForeignKey(Departement, on_delete=models.CASCADE, related_name='postes')

class Entreprise(models.Model):
    ifu = models.BigIntegerField()
    nom_entreprise = models.CharField(max_length=100)
    statut = models.CharField(max_length=70)

class Employe (models.Model):
    nom =models.CharField(max_length=100)
    prenom=models.CharField(max_length=100)
    age=models.IntegerField()
    sexe=models.CharField(max_length=10)
    email=models.EmailField()
    telephone=models.IntegerField(default=0)
    mot_passe=models.CharField(max_length=100)
    evaluations = models.ManyToManyField(Evaluation, through='Resultat')
    poste = models.ForeignKey(Poste, on_delete=models.CASCADE, related_name='employe')
    entreprise = models.ForeignKey(Entreprise, on_delete=models.CASCADE, related_name='employes')
def __str__(self):
        return self.nom + ' ' + self.prenom


class Resultat(models.Model):
    employe = models.ForeignKey(Employe, on_delete=models.CASCADE)
    evaluation = models.ForeignKey(Evaluation, on_delete=models.CASCADE)
    resultat = models.TextField()


class Conseiller(models.Model):
    nom =models.CharField(max_length=50)
    prenom=models.CharField(max_length=100)
    age=models.IntegerField()
    sexe=models.CharField(max_length=30)
    email=models.EmailField(max_length=100)
    telephone=models.IntegerField(default=0)
    mot_passe=models.CharField(max_length=50)
    rapports = models.ManyToManyField(Resultat, through='Rapport')



class Rapport(models.Model):
    conseiller=models.ForeignKey(Conseiller,on_delete=models.CASCADE)
    resultat=models.ForeignKey(Resultat,on_delete=models.CASCADE)
    contenu=models.TextField()
    plan_action=models.TextField()
