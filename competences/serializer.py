from .models import *
from rest_framework import serializers
from django.contrib.auth.models import User

class DomaineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Domaine
        fields = "__all__"

class EvaluationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Evaluation
        fields = "__all__"

class DepartementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Departement
        fields = "__all__"

class PosteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poste
        fields = "__all__"


class EntrepriseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entreprise
        fields = "__all__"

class EmployeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employe
        fields = "__all__"


class ResultatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resultat
        fields = "__all__"


class ConseillerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Conseiller
        fields = "__all__"


class RapportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rapport
        fields = "__all__"


