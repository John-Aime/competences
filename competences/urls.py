from django.urls import path, include
from.views import *
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'domaine', DomaineView, 'domaine' )
router.register(r'evaluation', EvaluationView, 'evaluation' )
router.register(r'departement', DepartementView, 'departement' )
router.register(r'employe', EmployeView, 'employe' )
router.register(r'Poste', PosteView, 'poste' )
router.register(r'entreprise', EntrepriseView, 'entreprise' )
router.register(r'rapport', RapportView, 'rapport' )
router.register(r'resultat', ResultatView, 'resultat' )
router.register(r'conseiller', ConseillerView, 'conseiller' )


urlpatterns = [
    path('api', include(router.urls)),
    
]